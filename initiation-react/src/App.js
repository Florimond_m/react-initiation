import React from 'react';
import logo from './logo.svg';
import './App.css';
import MultiButtons from './components/MultiButtons';
import ListRendering from './components/ListRendering';
import GreetingForm from './components/GreetingForm';
import JokeList from './components/JokeList';
import UserList from './components/UserList';



function App() {

  return (
    <div className="App">
      <header className="App-header">
      <MultiButtons  />
      <ListRendering />
      <GreetingForm/>
      <JokeList />
      <UserList />
        <a>
          Hello world !
        </a>
      </header>
    </div>
  );
}

export default App;
