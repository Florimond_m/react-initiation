import React, { useState } from 'react';

function MultiButtons() {
  const [clickCounts, setClickCounts] = useState({
    button1: 0,
    button2: 0,
    button3: 0
  });

  function handleClick(buttonId) {
    setClickCounts(prevCounts => ({
      ...prevCounts,
      [buttonId]: prevCounts[buttonId] + 1
    }));
  }

  return (
    <>
      <p>Button 1 clicked: {clickCounts.button1} times</p>
      <p>Button 2 clicked: {clickCounts.button2} times</p>
      <p>Button 3 clicked: {clickCounts.button3} times</p>
      <button onClick={() => handleClick('button1')}>button1</button>
      <button onClick={() => handleClick('button2')}>button2</button>
      <button onClick={() => handleClick('button3')}>Button3</button>
    </>
  );
}

export default MultiButtons;
