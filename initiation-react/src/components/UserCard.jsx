import React from 'react';

function UserCard({ user }) {
  const { first_name, last_name, email, address, phone, avatar } = user;

  return (
    <div className="user-card">
      <div className="user-card-front">
        <div className="user-name">
            <p>
            {first_name} {last_name}
            </p>
        </div>
        <div className="user-email">{email}</div>
      </div>
      <div className="user-card-back">
        <div className="user-avatar">
          <img src={avatar} alt="User Avatar" className="user-avatar-image" />
        </div>
        <div className="user-address">
          <p>{address.street_name} </p>
          <p>{address.city}</p>
          <p>{address.country}</p>
        </div>
        <div className="user-phone">{phone}</div>
      </div>
    </div>
  );
}

export default UserCard;


