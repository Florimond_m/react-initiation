import React from 'react';

function ListRendering() {
  const items = ['dog', 'cat', 'chicken', 'cow', 'sheep', 'horse'];

  return (
    <ul>
      {items.map((item, index) => (
        <li key={index}>{item}</li>
      ))}
    </ul>
  );
}

export default ListRendering;
