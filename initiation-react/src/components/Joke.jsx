import React from 'react';

function Joke({ joke }) {
  return (
    <div className="joke">
      <div className="joke-setup">{joke.setup}</div>
      <div className="joke-punchline">{joke.punchline}</div>
    </div>
  );
}

export default Joke;
